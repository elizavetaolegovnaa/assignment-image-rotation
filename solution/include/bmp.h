#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include <stdio.h>
#include <stdint.h>

enum readStatus {
    ReadOk = 0,
    ReadInvalidHeader,
    ReadOffsetError,
    ReadAllocError,
    ReadError,
};

enum readStatus fromBmp(FILE *in, struct image *img);

enum writeStatus {
    WriteOk = 0,
    WriteError,
};

enum writeStatus toBmp(FILE *out, const struct image *img);

static const char *const readErrToStr[] = {
        [ReadOk] = "OK",
        [ReadInvalidHeader] = "Invalid header",
        [ReadOffsetError] = "Error setting file cursor to data offset.",
        [ReadAllocError] = "Error allocating memory while reading.",
        [ReadError] = "Reading error."
};

static const char *const writeErrToStr[] = {
        [WriteOk] = "OK",
        [WriteError] = "Writing error."
};

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
