#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H

#include "image.h"

struct image rotate(struct image const source);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
