#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image newImage(uint32_t width, uint32_t height);

void deleteImage(struct image *);


#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
