#include "image.h"
#include <stdlib.h>

struct image newImage(uint32_t width, uint32_t height) {
    struct image newImage = {width, height, malloc(width * height * sizeof(struct pixel))};
    return newImage;
}

void deleteImage(struct image *img){
    free(img->data);
    img->data = NULL;
}
