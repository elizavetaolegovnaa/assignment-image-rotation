#include "bmp.h"
#include "transform.h"
#include <stdio.h>


int main(int argc, char **argv) {
    //checking if number of arguments is correct
    if (argc != 3) {
        fprintf(stderr, "Incorrect input arguments list.\n"
                        "This program expect launching with 2 arguments:"
                        "./image-transformer <source-image> <transformed-image>\n"
                        "Where:\n"
                        "    source-image - name of input image file\n"
                        "    transformed-image - name of output image file\n");
        return 1;
    }

    //opening input file
    FILE *input = fopen(argv[1], "r");
    if (input == NULL) {
        fprintf(stderr, "Error occurred while opening input file \"%s\".\n", argv[1]);
        return 1;
    }

    //reading image from input file
    struct image img;
    enum readStatus readStatus = fromBmp(input, &img);
    if (readStatus) {
        fprintf(stderr, "%s\n", readErrToStr[readStatus]);
        if (fclose(input)) {
            fprintf(stderr, "Error occurred while closing input file \"%s\".\n", argv[1]);
        }
        return readStatus;
    }

    //closing input file
    if (fclose(input)) {
        deleteImage(&img);
        fprintf(stderr, "Error occurred while closing input file \"%s\".\n", argv[1]);
        return 1;
    }

    //transforming image
    struct image newImg = rotate(img);
    deleteImage(&img);
    if (newImg.data == NULL) {
        fprintf(stderr, "Error allocating memory while rotating.\n");
        return 1;
    }

    //opening output file
    FILE *output = fopen(argv[2], "w");
    if (output == NULL) {
        deleteImage(&newImg);
        fprintf(stderr, "Error occurred while creating output file \"%s\".\n", argv[2]);
        return 1;
    }

    //writing rotated image to file
    enum writeStatus writeStatus = toBmp(output, &newImg);
    deleteImage(&newImg);
    if (writeStatus) {
        fprintf(stderr, "%s\n", writeErrToStr[writeStatus]);
        if (fclose(output)) {
            fprintf(stderr, "Error occurred while closing output file \"%s\".\n", argv[2]);
        }
        return writeStatus;
    }

    //closing input file
    if (fclose(output)) {
        fprintf(stderr, "Error occurred while closing output file \"%s\".\n", argv[2]);
        return 1;
    }

    printf("Finished successfully!\n");
    return 0;
}
