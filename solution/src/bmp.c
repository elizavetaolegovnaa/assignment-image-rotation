#include "bmp.h"

#define BMP_SIGNATURE_LE 0x4D42
#define BMP_SIGNATURE_BE 0x424D

struct __attribute__((packed)) bmpHeader {
    uint16_t bfType; //4D42/424D (little-endian/big-endian).
    uint32_t bfileSize; //Размер файла в байтах.
    uint32_t bfReserved; //Зарезервирован и должен содержать ноль.
    uint32_t bOffBits; //Положение пиксельных данных относительно начала данной структуры (в байтах).
    uint32_t biSize; //Размер данной структуры в байтах
    uint32_t biWidth; //Ширина растра в пикселях.
    uint32_t biHeight; //Целое число со знаком, содержащее два параметра:
    //высота растра в пикселях (абсолютное значение числа)
    //и порядок следования строк в двумерных массивах (знак числа).
    uint16_t biPlanes; //В BMP допустимо только значение 1.
    uint16_t biBitCount; //Количество бит на пиксель.
    uint32_t biCompression; //Указывает на способ хранения пикселей.
    uint32_t biSizeImage; //Размер пиксельных данных в байтах.
    uint32_t biXPelsPerMeter; //Количество пикселей на метр по горизонтали и вертикали.
    uint32_t biYPelsPerMeter; //Количество пикселей на метр по горизонтали и вертикали.
    uint32_t biClrUsed; //Размер таблицы цветов в ячейках.
    uint32_t biClrImportant; //Количество ячеек от начала таблицы цветов до последней используемой (включая её саму).
};

static uint32_t getPadding(uint32_t width) {
    return width % 4;
}

static struct bmpHeader newBmpHeader(struct image img) {
    struct bmpHeader header = {
            BMP_SIGNATURE_LE,
            0,
            0,
            sizeof(struct bmpHeader),
            sizeof(struct bmpHeader) - 14,
            img.width,
            img.height,
            1,
            sizeof(struct pixel) * 8,
            0,
            0,
            0,
            0,
            0,
            0
    };
    header.biSizeImage = (img.width * sizeof(struct pixel) + getPadding(img.width)) * img.height;
    header.bfileSize = header.biSizeImage + header.bOffBits;
    return header;
}

static int incorrectBmpHeaderCheck(const struct bmpHeader *header) {
    //24 by task
    if (header->biBitCount != 24) {
        fprintf(stderr, "BMP\'s Bits per pixel header field isn't equal to 24!\n");
        return 1;
    }
    //part of standard
    if (header->biPlanes != 1) {
        fprintf(stderr, "BMP standard isn't met!\nPlanes header field isn't equal to 1!\n");
        return 1;
    }
    //little-endian
    if (header->bfType != BMP_SIGNATURE_LE) {
        fprintf(stderr, "File isn't in little-endian!\n");
        if (header->bfType != BMP_SIGNATURE_BE) {
            fprintf(stderr, "File isn't BMP!\n");
        }
        fprintf(stderr, "File is in big-endian!\n");
        return 1;
    }

    return 0;
}

enum readStatus fromBmp(FILE *in, struct image *img) {
    struct bmpHeader header;

    if (fread(&header, sizeof(struct bmpHeader), 1, in) != 1) {
        return ReadError;
    }

    if (incorrectBmpHeaderCheck(&header)) {
        return ReadInvalidHeader;
    }

    if (fseek(in, header.bOffBits, SEEK_SET)) {
        fprintf(stderr, "Error setting file cursor to pixels offset.");
        return ReadOffsetError;
    }

    *img = newImage(header.biWidth, header.biHeight);
    if (img->data == NULL) {
        return ReadAllocError;
    }
    //image read from file
    const uint32_t padding = getPadding(header.biWidth);
    if (header.biHeight) {
        for (int64_t  i = (int64_t) header.biHeight - 1; i >= 0; --i) {
            if (fread(img->data + (i * header.biWidth), sizeof(struct pixel), header.biWidth, in) !=
                header.biWidth ||
                fseek(in, padding, SEEK_CUR)) {
                deleteImage(img);
                return ReadError;
            }
        }
    }
    return ReadOk;
}

enum writeStatus toBmp(FILE *out, const struct image *img) {
    struct bmpHeader header = newBmpHeader(*img);
    
    if (fwrite(&header, sizeof(struct bmpHeader), 1, out) != 1) {
        return WriteError;
    }

    //image write to file
    const uint32_t padding = getPadding(header.biWidth);
	const uint8_t buffer[3] = {0, 0, 0};
    if (header.biHeight) {
        for (int64_t i = (int64_t) header.biHeight - 1; i >= 0; --i) {
            if (fwrite(img->data + (i * header.biWidth), sizeof(struct pixel), header.biWidth, out) !=
                header.biWidth ||
                fwrite(buffer, sizeof(uint8_t), padding, out) != padding) {
                return WriteError;
            }
        }
    }
    return WriteOk;
}
