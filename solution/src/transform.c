#include "transform.h"
#include <stdlib.h>

static uint64_t getPixelPos(uint64_t imgWidth, uint64_t x, uint64_t y) {
    return y * imgWidth + x;
}

struct image rotate(struct image source) {
    struct image res = newImage(source.height, source.width);
    if (res.data == NULL) {
        res.height = res.width = 0;
        return res;
    }
    for (uint64_t i = 0; i < source.height; ++i) {
        for (uint64_t j = 0; j < source.width; ++j) {
            uint64_t resY = source.width - j - 1;
            uint64_t resX = i;
            res.data[getPixelPos(res.width, resX, resY)] = source.data[getPixelPos(source.width, j, i)];
        }
    }

    return res;
}
